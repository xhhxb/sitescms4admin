package com.sites.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCompany<M extends BaseCompany<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Integer getId() {
		return getInt("id");
	}
	
	/**
	 * 名称
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 名称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}
	
	/**
	 * 地址
	 */
	public void setAddress(java.lang.String address) {
		set("address", address);
	}
	
	/**
	 * 地址
	 */
	public java.lang.String getAddress() {
		return getStr("address");
	}
	
	/**
	 * 邮政编码
	 */
	public void setPostCode(java.lang.String postCode) {
		set("postCode", postCode);
	}
	
	/**
	 * 邮政编码
	 */
	public java.lang.String getPostCode() {
		return getStr("postCode");
	}
	
	/**
	 * 联系人
	 */
	public void setContacts(java.lang.String contacts) {
		set("contacts", contacts);
	}
	
	/**
	 * 联系人
	 */
	public java.lang.String getContacts() {
		return getStr("contacts");
	}
	
	/**
	 * 手机号
	 */
	public void setMobile(java.lang.String mobile) {
		set("mobile", mobile);
	}
	
	/**
	 * 手机号
	 */
	public java.lang.String getMobile() {
		return getStr("mobile");
	}
	
	/**
	 * 座机
	 */
	public void setTelephone(java.lang.String telephone) {
		set("telephone", telephone);
	}
	
	/**
	 * 座机
	 */
	public java.lang.String getTelephone() {
		return getStr("telephone");
	}
	
	/**
	 * 传真
	 */
	public void setFax(java.lang.String fax) {
		set("fax", fax);
	}
	
	/**
	 * 传真
	 */
	public java.lang.String getFax() {
		return getStr("fax");
	}
	
	/**
	 * QQ
	 */
	public void setQq(java.lang.String qq) {
		set("qq", qq);
	}
	
	/**
	 * QQ
	 */
	public java.lang.String getQq() {
		return getStr("qq");
	}
	
	/**
	 * 邮箱
	 */
	public void setEmail(java.lang.String email) {
		set("email", email);
	}
	
	/**
	 * 邮箱
	 */
	public java.lang.String getEmail() {
		return getStr("email");
	}
	
	/**
	 * logo图片地址
	 */
	public void setLogo(java.lang.Integer logo) {
		set("logo", logo);
	}
	
	/**
	 * logo图片地址
	 */
	public java.lang.Integer getLogo() {
		return getInt("logo");
	}
	
	/**
	 * 微信图片地址
	 */
	public void setWeChatImg(java.lang.Integer weChatImg) {
		set("weChatImg", weChatImg);
	}
	
	/**
	 * 微信图片地址
	 */
	public java.lang.Integer getWeChatImg() {
		return getInt("weChatImg");
	}
	
	/**
	 * 营业执照
	 */
	public void setLicense(java.lang.String license) {
		set("license", license);
	}
	
	/**
	 * 营业执照
	 */
	public java.lang.String getLicense() {
		return getStr("license");
	}
	
	/**
	 * 网站地址
	 */
	public void setWebsite(java.lang.String website) {
		set("website", website);
	}
	
	/**
	 * 网站地址
	 */
	public java.lang.String getWebsite() {
		return getStr("website");
	}
	
	/**
	 * 其他信息
	 */
	public void setRemark(java.lang.String remark) {
		set("remark", remark);
	}
	
	/**
	 * 其他信息
	 */
	public java.lang.String getRemark() {
		return getStr("remark");
	}
	
	/**
	 * 所属站点
	 */
	public void setSiteId(java.lang.Integer siteId) {
		set("siteId", siteId);
	}
	
	/**
	 * 所属站点
	 */
	public java.lang.Integer getSiteId() {
		return getInt("siteId");
	}
	
}

