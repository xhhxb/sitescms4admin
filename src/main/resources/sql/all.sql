#namespace("account")
	#include("account.sql")
#end

#namespace("role")
	#include("role.sql")
#end

#namespace("permission")
	#include("permission.sql")
#end

#namespace("site")
	#include("site.sql")
#end

#namespace("column")
	#include("column.sql")
#end

#namespace("article")
	#include("article.sql")
#end

#namespace("accessLog")
	#include("accessLog.sql")
#end

#namespace("company")
	#include("company.sql")
#end
