/*
 * 3.0.0版本升级SQL
 */
alter table site add editor varchar(100) null comment '编辑器，wangEditor、TinyMCE' after videoSuffix;
--调整数据库编码
ALTER TABLE accesslog MODIFY COLUMN parameter text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
ALTER TABLE accesslog MODIFY COLUMN remark varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '其他信息';
ALTER TABLE accesslog MODIFY COLUMN `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '访问类型（cds、cms）';
ALTER TABLE accesslog MODIFY COLUMN ip varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '访问者ip地址';
ALTER TABLE accesslog MODIFY COLUMN visitor varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '访问者姓名，cms访问记录用户登录名';
ALTER TABLE accesslog MODIFY COLUMN actionKey varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'actionKey';

ALTER TABLE account MODIFY COLUMN nickName varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '昵称';
ALTER TABLE account MODIFY COLUMN userName varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '登录名';
ALTER TABLE account MODIFY COLUMN password varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '密码';
ALTER TABLE account MODIFY COLUMN status varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' NULL COMMENT '状态（0正常，1锁定，2删除）';

ALTER TABLE article MODIFY COLUMN `type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE article MODIFY COLUMN status varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '状态（0默认发布，1删除，2草稿）';
ALTER TABLE article MODIFY COLUMN isTop varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '置顶';
ALTER TABLE article MODIFY COLUMN files varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '附件id串，多个以,分割';

ALTER TABLE `column` MODIFY COLUMN name varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE `column` MODIFY COLUMN enName varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '栏目英文标识，站点内唯一';
ALTER TABLE `column` MODIFY COLUMN children varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '子栏目id拼接的字符串，以|拼接';
ALTER TABLE `column` MODIFY COLUMN status varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '状态，1启用，2禁用';

ALTER TABLE files MODIFY COLUMN `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '文件类型：annex附件，thumbnail缩略图，Album相册';
ALTER TABLE files MODIFY COLUMN name varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '文件名';
ALTER TABLE files MODIFY COLUMN originalName varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '原始文件名';
ALTER TABLE files MODIFY COLUMN `path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '文件保存路径';
ALTER TABLE files MODIFY COLUMN contentType varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE files MODIFY COLUMN remark varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '文件说明';

ALTER TABLE permission MODIFY COLUMN actionKey varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE permission MODIFY COLUMN controller varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE permission MODIFY COLUMN remark varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE permission MODIFY COLUMN status varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '1' NULL COMMENT '启用状态';

ALTER TABLE `role` MODIFY COLUMN name varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '角色名';
ALTER TABLE `role` MODIFY COLUMN remark varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注';

ALTER TABLE site MODIFY COLUMN siteName varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '站点中文名称';
ALTER TABLE site MODIFY COLUMN siteSign varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '站点的英文标示，唯一不可重复';
ALTER TABLE site MODIFY COLUMN siteUrl varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '站点地址';
ALTER TABLE site MODIFY COLUMN siteDes varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '站点描述';
ALTER TABLE site MODIFY COLUMN cdsAccessLog varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' NULL COMMENT 'cds访问日志（默认0不启用，1启用）';
ALTER TABLE site MODIFY COLUMN cmsAccessLog varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' NULL COMMENT 'cms访问日志（默认0不启用，1启用）';
ALTER TABLE site MODIFY COLUMN imgSuffix varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '支持图片后缀，以|分割';
ALTER TABLE site MODIFY COLUMN fileSuffix varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '支持文件后缀，以|分割';
ALTER TABLE site MODIFY COLUMN videoSuffix varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '支持视频后缀，以|分割';
ALTER TABLE site MODIFY COLUMN editor varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '编辑器，wangEditor、TinyMCE';
ALTER TABLE site MODIFY COLUMN status varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '是否是当前站点，1标识是，2标识不是，只能有一个1';
ALTER TABLE site MODIFY COLUMN icpNo varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'ICP备案号';
ALTER TABLE site MODIFY COLUMN securityNo varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '公安备案号';

/*
 * 2.2.0版本升级SQL
 */
--文章表字段类型调整
ALTER TABLE article MODIFY COLUMN content MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '文章类型（1普通文章，2图集）';
ALTER TABLE article MODIFY COLUMN contentText MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '纯文本格式的文章内容';
--文章表新增字段
ALTER TABLE article ADD summary TEXT NULL COMMENT '摘要';
ALTER TABLE article ADD source varchar(512) NULL COMMENT '来源';
ALTER TABLE article ADD author varchar(128) NULL COMMENT '作者';
ALTER TABLE article ADD tag varchar(512) NULL COMMENT '标签';
ALTER TABLE article ADD keywords varchar(512) NULL COMMENT '关键字';
ALTER TABLE article ADD startTime DATETIME NULL COMMENT '发布的起始时间';
ALTER TABLE article ADD endTime DATETIME NULL COMMENT '失效时间';
ALTER TABLE article MODIFY COLUMN subtitle varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '子标题';
ALTER TABLE article MODIFY COLUMN title varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '标题';
ALTER TABLE article MODIFY COLUMN `column` int NULL COMMENT '所属栏目';
--【可选】下面这些是调整字段顺序的，根据个人喜好选择是否执行
ALTER TABLE article CHANGE summary summary TEXT NULL COMMENT '摘要' AFTER keywords;
ALTER TABLE article CHANGE source source varchar(512) NULL COMMENT '来源' AFTER author;
ALTER TABLE article CHANGE author author varchar(128) NULL COMMENT '作者' AFTER `column`;
ALTER TABLE article CHANGE tag tag varchar(512) NULL COMMENT '标签' AFTER source;
ALTER TABLE article CHANGE keywords keywords varchar(512) NULL COMMENT '关键字' AFTER tag;
ALTER TABLE article CHANGE summary summary text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '摘要' AFTER subtitle;
ALTER TABLE article CHANGE source source varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '来源' AFTER author;
ALTER TABLE article CHANGE tag tag varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '标签' AFTER source;
ALTER TABLE article CHANGE keywords keywords varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '关键字' AFTER tag;
ALTER TABLE article CHANGE startTime startTime datetime NULL COMMENT '发布的起始时间' AFTER files;
ALTER TABLE article CHANGE endTime endTime datetime NULL COMMENT '失效时间' AFTER startTime;
--站点表新增字段
ALTER TABLE site ADD siteTitle varchar(512) NULL COMMENT 'SEO标题';
ALTER TABLE site ADD siteKeys varchar(1024) NULL COMMENT 'SEO关键字';
ALTER TABLE site ADD icpNo varchar(512) NULL COMMENT 'ICP备案号';
ALTER TABLE site ADD securityNo varchar(512) NULL COMMENT '公安备案号';
--【可选】下面这些是调整字段顺序的，根据个人喜好选择是否执行
ALTER TABLE site CHANGE siteTitle siteTitle varchar(512) NULL COMMENT 'SEO标题' AFTER siteUrl;
ALTER TABLE site CHANGE siteKeys siteKeys varchar(1024) NULL COMMENT 'SEO关键字' AFTER siteTitle;
--新增公司表
CREATE TABLE `company` (
                           `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
                           `name` varchar(100) NOT NULL COMMENT '名称',
                           `address` varchar(512) DEFAULT NULL COMMENT '地址',
                           `postCode` varchar(128) DEFAULT NULL COMMENT '邮政编码',
                           `contacts` varchar(128) DEFAULT NULL COMMENT '联系人',
                           `mobile` varchar(128) DEFAULT NULL COMMENT '手机号',
                           `telephone` varchar(128) DEFAULT NULL COMMENT '座机',
                           `fax` varchar(128) DEFAULT NULL COMMENT '传真',
                           `qq` varchar(128) DEFAULT NULL COMMENT 'QQ',
                           `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
                           `logo` int DEFAULT NULL COMMENT 'logo图片地址',
                           `weChatImg` int DEFAULT NULL COMMENT '微信图片地址',
                           `license` varchar(512) DEFAULT NULL COMMENT '营业执照',
                           `website` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '网站地址',
                           `remark` varchar(512) DEFAULT NULL COMMENT '其他信息',
                           `siteId` int DEFAULT NULL COMMENT '所属站点',
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='公司信息';

/*
 * 2.0.6版本升级SQL
 */
ALTER TABLE sitescms.accesslog DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.account DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.accountrole DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.article DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.`column` DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.files DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.permission DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.`role` DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.rolepermission DEFAULT CHARSET=utf8mb4;
ALTER TABLE sitescms.site DEFAULT CHARSET=utf8mb4;

/*
 * 2.0.4版本升级SQL
 */
--栏目表添加排序列和注释
ALTER TABLE `column` MODIFY COLUMN parent int NULL COMMENT '父栏目id';
ALTER TABLE `column` ADD `order` INT DEFAULT 1 NULL COMMENT '栏目排序，小的在前';
ALTER TABLE `column` CHANGE `order` `order` INT DEFAULT 1 NULL COMMENT '栏目排序，小的在前' AFTER status;
ALTER TABLE `column` MODIFY COLUMN status varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '状态，1启用，2禁用';
--站点表添加不可为空约束
ALTER TABLE site MODIFY COLUMN imgMaxSize int NOT NULL COMMENT '上传图片最大限定，单位M';
ALTER TABLE site MODIFY COLUMN fileMaxSize int NOT NULL COMMENT '上传文件最大限定，单位M';
ALTER TABLE site MODIFY COLUMN videoMaxSize int NOT NULL COMMENT '上传视频最大限定，单位M';
ALTER TABLE site MODIFY COLUMN fileSuffix varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支持文件后缀，以|分割';
ALTER TABLE site MODIFY COLUMN videoSuffix varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支持视频后缀，以|分割';


/*
 * 2.0.0版本升级SQL
 */
ALTER TABLE sitescms.site ADD imgMaxSize INT NULL COMMENT '上传图片最大限定，单位M';
ALTER TABLE sitescms.site CHANGE imgMaxSize imgMaxSize INT NULL COMMENT '上传图片最大限定，单位M' AFTER cmsAccessLog;
ALTER TABLE sitescms.site ADD videoMaxSize INT NULL COMMENT '上传视频最大限定，单位M';
ALTER TABLE sitescms.site CHANGE videoMaxSize videoMaxSize INT NULL COMMENT '上传视频最大限定，单位M' AFTER fileMaxSize;
ALTER TABLE sitescms.site MODIFY COLUMN id INT auto_increment NOT NULL;
ALTER TABLE sitescms.site MODIFY COLUMN fileMaxSize INT NULL COMMENT '上传文件最大限定，单位M';
ALTER TABLE sitescms.site ADD imgSuffix varchar(256) NOT NULL COMMENT '支持图片后缀，以|分割';
ALTER TABLE sitescms.site CHANGE imgSuffix imgSuffix varchar(256) NOT NULL COMMENT '支持图片后缀，以|分割' AFTER videoMaxSize;
ALTER TABLE sitescms.site ADD videoSuffix varchar(256) NULL COMMENT '支持视频后缀，以|分割';
ALTER TABLE sitescms.site CHANGE videoSuffix videoSuffix varchar(256) NULL COMMENT '支持视频后缀，以|分割' AFTER fileSuffix;
